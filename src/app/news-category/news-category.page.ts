import { Router } from '@angular/router';
import { NewsService } from './../services/news/news.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-category',
  templateUrl: './news-category.page.html',
  styleUrls: ['./news-category.page.scss'],
})
export class NewsCategoryPage implements OnInit {

  data: any;
  
  constructor(private newsService: NewsService,
              private router: Router) { }

  ngOnInit() {
    this.newsService
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      //console.log(data);
      this.data = data;
    });
  }

}
